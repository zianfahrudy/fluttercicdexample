// Copyright (c) 2021, Very Good Ventures
// https://verygood.ventures
//
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.

import 'dart:async';
import 'dart:developer';

import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_gitlab_cicd/core/di/injection_container.dart' as di;

import 'package:flutter_gitlab_cicd/app/app_bloc_observer.dart';
import 'package:flutter_gitlab_cicd/env/flavor.dart';

import 'app/app.dart';
import 'env/config.dart';

void main() {
  runZonedGuarded(
    () async {
      WidgetsFlutterBinding.ensureInitialized();

      FlutterError.onError = (details) {
        log(details.exceptionAsString(), stackTrace: details.stack);
      };
      FlavorSettings.production();

      // init injection, firebase, etc
      await di.init();

      BlocOverrides.runZoned(() => runApp(const App()),
          blocObserver: AppBlocObserver());

      ///[console] flavor running
      if (!kReleaseMode) {
        final settings = Config.getInstance();
        log('🚀 APP FLAVOR NAME      : ${settings.flavorName}', name: 'ENV');
        log('🚀 APP API_BASE_URL     : ${settings.apiBaseUrl}', name: 'ENV');
        log('🚀 APP API_SENTRY       : ${settings.apiSentry}', name: 'ENV');
      }
    },
    (error, stackTrace) => log(error.toString(), stackTrace: stackTrace),
  );
}
