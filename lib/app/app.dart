// Copyright (c) 2021, Very Good Ventures
// https://verygood.ventures
//
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gitlab_cicd/app/pages/login/login_screen.dart';
import 'package:flutter_gitlab_cicd/core/component/bloc/login/login_bloc.dart';
import 'package:flutter_gitlab_cicd/core/di/injection_container.dart';
import 'package:get/get.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final loginBloc = sl<LoginBloc>();
    return BlocProvider(
      create: (context) => loginBloc,
      child: GetMaterialApp(
        theme: ThemeData(
          appBarTheme: const AppBarTheme(color: Color(0xFF13B9FF)),
          colorScheme: ColorScheme.fromSwatch(
            accentColor: const Color(0xFF13B9FF),
          ),
        ),
        home: const LoginScreen(),
      ),
    );
  }
}
