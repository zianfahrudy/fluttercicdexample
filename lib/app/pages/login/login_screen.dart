import 'package:flutter/material.dart';
import 'package:flutter_gitlab_cicd/app/pages/login/view/login_view.dart';
import 'package:flutter_gitlab_cicd/core/component/bloc/login/login_bloc.dart';
import 'package:flutter_gitlab_cicd/core/di/injection_container.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final loginBloc = sl<LoginBloc>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        body: LoginView(loginBloc: loginBloc));
  }
}
