import 'package:dartz/dartz.dart';
import 'package:flutter_gitlab_cicd/core/component/bloc/login/data/models/request/login_body.dart';
import 'package:flutter_gitlab_cicd/core/component/domains/entities/response/login_model_entity.dart';
import 'package:flutter_gitlab_cicd/core/widgets/failure.dart';

abstract class UserRepository {
  Stream<Either<Failure, LoginModelEntity>> requestLogin(LoginBody body);
}
