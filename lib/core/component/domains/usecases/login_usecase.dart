import 'package:dartz/dartz.dart';
import 'package:flutter_gitlab_cicd/core/component/bloc/login/data/models/request/login_body.dart';
import 'package:flutter_gitlab_cicd/core/component/domains/entities/response/login_model_entity.dart';
import 'package:flutter_gitlab_cicd/core/component/domains/repository/user_repository.dart';
import 'package:flutter_gitlab_cicd/core/interactor/usecase.dart';
import 'package:flutter_gitlab_cicd/core/widgets/failure.dart';

class LoginUseCase extends UseCase<LoginModelEntity, LoginBody> {
  final UserRepository repository;

  LoginUseCase(this.repository);

  @override
  Stream<Either<Failure, LoginModelEntity>> build(LoginBody params) {
    return repository.requestLogin(params);
  }
}
