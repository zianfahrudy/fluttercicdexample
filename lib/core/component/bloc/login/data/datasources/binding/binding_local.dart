import 'package:flutter_gitlab_cicd/core/component/bloc/login/data/datasources/binding_datasources.dart';
import 'package:flutter_gitlab_cicd/core/component/bloc/login/data/models/request/login_body.dart';
import 'package:flutter_gitlab_cicd/core/component/domains/entities/response/login_model_entity.dart';
import 'package:flutter_gitlab_cicd/core/storage/local_storage.dart';

class BindingLocal implements BindingDataSources {
  final SharedPrefs? prefs;

  BindingLocal(this.prefs);

  @override
  Stream<LoginModelEntity> requestLogin(LoginBody body) {
    throw UnimplementedError();
  }
}
