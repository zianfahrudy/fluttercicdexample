import 'package:flutter_gitlab_cicd/core/component/bloc/login/data/models/request/login_body.dart';
import 'package:flutter_gitlab_cicd/core/component/domains/entities/response/login_model_entity.dart';

abstract class BindingDataSources {
  /// [AUTH]
  ///
  Stream<LoginModelEntity> requestLogin(LoginBody body);
}
