// ignore_for_file: overridden_fields, annotate_overrides

import 'package:flutter_gitlab_cicd/core/component/domains/entities/request/login_body_entity.dart';
import 'package:json_annotation/json_annotation.dart';

part 'login_body.g.dart';

@JsonSerializable(
  createFactory: false,
)
class LoginBody extends LoginBodyEntity {
  const LoginBody({this.email, this.password})
      : super(email: email, password: password);
  final String? email;
  final String? password;

  Map<String, dynamic> toJson() => _$LoginBodyToJson(this);
}
