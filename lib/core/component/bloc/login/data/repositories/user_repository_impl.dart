import 'package:dartz/dartz.dart';
import 'package:flutter_gitlab_cicd/core/component/bloc/login/data/datasources/common/base_datasource_factory.dart';
import 'package:flutter_gitlab_cicd/core/component/bloc/login/data/datasources/common/datasource_factory.dart';
import 'package:flutter_gitlab_cicd/core/component/bloc/login/data/models/request/login_body.dart';
import 'package:flutter_gitlab_cicd/core/component/domains/entities/response/login_model_entity.dart';
import 'package:flutter_gitlab_cicd/core/component/domains/repository/user_repository.dart';
import 'package:flutter_gitlab_cicd/core/widgets/failure.dart';

class UserRepositoryImpl implements UserRepository {
  late final BindingDataSourceFactory _bindingDataSource;

  UserRepositoryImpl(this._bindingDataSource);

  @override
  Stream<Either<Failure, LoginModelEntity>> requestLogin(LoginBody body) {
    return _bindingDataSource
        .createData(DataSourceState.network)!
        .requestLogin(body)
        .map((event) => Right(event));
  }
}
