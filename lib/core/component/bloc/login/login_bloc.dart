import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_gitlab_cicd/core/component/bloc/login/data/models/request/login_body.dart';
import 'package:flutter_gitlab_cicd/core/component/domains/entities/response/login_model_entity.dart';
import 'package:flutter_gitlab_cicd/core/component/domains/usecases/login_usecase.dart';
import 'package:flutter_gitlab_cicd/core/const/key_const.dart';
import 'package:flutter_gitlab_cicd/core/di/injection_container.dart';
import 'package:flutter_gitlab_cicd/core/storage/local_storage.dart';
import 'package:flutter_gitlab_cicd/core/widgets/failure.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final SharedPrefs prefs = sl<SharedPrefs>();

  final LoginUseCase loginUseCase;

  LoginBloc(this.loginUseCase) : super(LoginInitial()) {
    on<OnLoginEvent>((event, emit) async {
      emit(LoginLoading());

      final Stream<Either<Failure, LoginModelEntity>> response =
          loginUseCase.execute(event.body);

      await for (final eventRes in response) {
        eventRes.fold((error) {
          emit(LoginFailure(error.message));
        }, (values) {
          prefs.putString(
              KeyConstants.keyAccessToken, values.data!.accessToken!);
          emit(LoginSuccess());
        });
      }
    });
  }
}
